#ifndef INTROSORT_CPP
#define INTROSORT_H
#include <functional>
#include <math.h>

#include <cmath>
#include <algorithm>
#include "selectionsort.h"

#include "shellsort.h"


/*------------------------------------------------------------------------------------------------------
 * Introsort algorithm || Assignment 2 || STE6274 Game Design
 * Created by Renate Karlsen || https://bitbucket.org/ftwrenate/ste6274_assignment_2_introsort ||
 * -----------------------------------------------------------------------------------------------------
 */

namespace mylib {

    // Make introsort and introsort_loop accessible no matter which order the method compiles.
    template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
    void introsort( RandomAccessIterator first, RandomAccessIterator last, Compare comp = Compare());

    template <class RandomAccessIterator, class Compare, class Distance>
    void introsort_loop(RandomAccessIterator first, RandomAccessIterator last, Compare comp, Distance depth_limit);

    // Compute the median of 3, and swap values.
    template <class RandomAccessIterator>
    typename RandomAccessIterator::value_type Mo3(RandomAccessIterator first, RandomAccessIterator last)
    {
         using value_type =  typename RandomAccessIterator::value_type;
         value_type mo3 = *(first + (last-first)/2);
         value_type fval = *first;
         value_type lval = *(last-1);

         if (lval < fval)
              std::swap(fval,lval);
         if (mo3 < fval )
              std::swap(mo3, fval);
         if(lval < mo3)
              std::swap(lval, mo3);

        return mo3;
     }

    // Introsort_loop goes through the whole list.
    template <class RandomAccessIterator, class Compare, class Distance>
    void introsort_loop(RandomAccessIterator first, RandomAccessIterator last, Compare comp, Distance depth_limit)
    {

        using value_type =  typename RandomAccessIterator::value_type;

        while (first - last > 0)
        {
            if (depth_limit == 0)
            {
                selection_sort(first,last,comp); // Call the selection_sort method
                return;
            }
            --depth_limit;


            RandomAccessIterator cut = std::partition( first, last,
                                                       std::bind(std::less<value_type>(),std::placeholders::_1,Mo3(first, last))
                                                       );


            introsort_loop(cut,last,comp,depth_limit);
            last = cut;


        }

    }

    // Call the introsort_loop and shellsort method.
    template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
    void introsort( RandomAccessIterator first, RandomAccessIterator last, Compare comp = Compare())
    {

        introsort_loop(first,last,comp,(2*std::floor(std::log2(last-first))));
        shellSort(first,last,comp);

    }
}

#endif // INTROSORT_H

